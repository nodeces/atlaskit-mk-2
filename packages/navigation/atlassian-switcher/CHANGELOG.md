# @atlaskit/atlassian-switcher

## 0.2.1
- [patch] [94acafec27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/94acafec27):

  - Adds the error page according to the designs.

## 0.2.0
- [minor] [9d5cc39394](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d5cc39394):

  - Dropped ES5 distributables from the typescript packages

## 0.1.4
- [patch] [b08df363b7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b08df363b7):

  - Add atlassian-switcher prefetch trigger in global-navigation

## 0.1.3
- [patch] [269cd93118](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/269cd93118):

  - Progressive loading and prefetch primitives

## 0.1.2
- [patch] [6ca66fceac](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6ca66fceac):

  - Add enableSplitJira to allow multiple jira link displayed if there are jira products

## 0.1.1
- Updated dependencies [76299208e6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/76299208e6):
  - @atlaskit/button@10.1.3
  - @atlaskit/icon@16.0.4
  - @atlaskit/analytics-gas-types@3.2.5
  - @atlaskit/analytics-namespaced-context@2.2.1
  - @atlaskit/docs@7.0.0
  - @atlaskit/analytics-next@4.0.0
  - @atlaskit/drawer@3.0.0
  - @atlaskit/item@9.0.0
  - @atlaskit/logo@10.0.0
  - @atlaskit/lozenge@7.0.0
  - @atlaskit/navigation@34.0.0
  - @atlaskit/theme@8.0.0

## 0.1.0
- [minor] [6ee7b60c4a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6ee7b60c4a):

  - Create generic switcher for satellite products

## 0.0.9
- [patch] [e7fa9e1308](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7fa9e1308):

  - Fixing icon imports

## 0.0.8
- [patch] [ebfdf1e915](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ebfdf1e915):

  - Update sections and grouping according to updated designs

## 0.0.7
- [patch] [8a70a0db9f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8a70a0db9f):

  - SSR compatibility fix

## 0.0.6
- [patch] [3437ac9990](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3437ac9990):

  - Firing events according to minimum event spec

## 0.0.5
- [patch] [9184dbf08b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9184dbf08b):

  - Fixing package.json issue with atlassian-switcher

## 0.0.4
- [patch] [95d9a94bd0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/95d9a94bd0):

  - Adding root index for atlassian-switcher

## 0.0.3
- [patch] [b56ca0131d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b56ca0131d):

  - Attempting to fix flow issue where atlassian-switcher is not recognized

## 0.0.2
- [patch] [235f937d66](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/235f937d66):

  - Initial package release

## 0.0.1
- [patch] [25921b9e50](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/25921b9e50):

  - Adding AtlassianSwitcher component
