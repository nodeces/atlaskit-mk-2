# @atlaskit/share

## 0.2.0
- [minor] [9d5cc39394](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d5cc39394):

  - Dropped ES5 distributables from the typescript packages

## 0.1.18
- Updated dependencies [76299208e6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/76299208e6):
  - @atlaskit/button@10.1.3
  - @atlaskit/icon@16.0.4
  - @atlaskit/user-picker@1.0.25
  - @atlaskit/util-data-test@10.2.3
  - @atlaskit/util-service-support@3.1.1
  - @atlaskit/docs@7.0.0
  - @atlaskit/field-text-area@5.0.0
  - @atlaskit/form@5.1.8
  - @atlaskit/inline-dialog@10.0.0
  - @atlaskit/section-message@2.0.0
  - @atlaskit/theme@8.0.0
  - @atlaskit/tooltip@13.0.0

## 0.1.17
- [patch] [af38e4649a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/af38e4649a):

  - exposed shareContentType prop

## 0.1.16
- [patch] [aca247a78b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/aca247a78b):

  - Exposed shareFormTitle prop to customise Share Form title
  - Removed object type from Copy link button

## 0.1.15
- [patch] [312572b5f8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/312572b5f8):

  - FS-3618 consume configuration in the UserPickerField

## 0.1.14
- [patch] [4d3226b06b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4d3226b06b):

  - exposed trigger button appearance prop

## 0.1.13
- [patch] [3f5be35333](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/3f5be35333):

  - Fixed errors and warnings in elements/share unit tests

## 0.1.12
- [patch] [376926523b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/376926523b):

  - Explosed buttonStyle prop to ShareDialogContainer

## 0.1.11
- [patch] [7e809344eb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7e809344eb):

  - Modified share web component to send a single atlOriginId

## 0.1.10
- Updated dependencies [4af5bd2a58](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4af5bd2a58):
  - @atlaskit/editor-test-helpers@7.0.0

## 0.1.9
- [patch] [7569356ab3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7569356ab3):

  - FS-3417 add email warning, save intermediate state if click outside

## 0.1.8
- [patch] [d1fbdc3a35](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d1fbdc3a35):

  - enable noImplicitAny for share. fix related issues

## 0.1.7
- [patch] [8e0ea83f02](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8e0ea83f02):

  - Added ShareDialogContainer component

## 0.1.6
- [patch] [1d284d2437](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1d284d2437):

  - FS-3417 added ShareButton, ShareDialogTrigger components to @atlaskit/share

## 0.1.5
- [patch] [2f73eeac57](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2f73eeac57):

  - Added ShareServiceClient and unit test
- [patch] [8c905d11b7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8c905d11b7):

  - Added share service client

## 0.1.4
- [patch] [b752299534](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b752299534):

  - Added capabilities info message in ShareForm

## 0.1.3
- [patch] [42bfdcf8ed](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/42bfdcf8ed):

  - Added CopyLinkButton component and integrated into ShareForm

## 0.1.2
- [patch] [48856cfa79](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/48856cfa79):

  - Added IdentityClient and unit tests

## 0.1.1
- [patch] [64bf358](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/64bf358):

  - FS-3416 add ShareForm component to @atlaskit/share

## 0.1.0
- [minor] [891e116](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/891e116):

  - FS-3291 add share skeleton
