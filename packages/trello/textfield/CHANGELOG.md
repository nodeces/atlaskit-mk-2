# @nachos/textfield

## 0.0.3
- [patch] [697044626f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/697044626f):

  - Bumping to follow all the other bumping in AK

## 0.0.2
- [patch] [03ff6e99e2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/03ff6e99e2):

  - First release of Nachos textfield! 🎉 This is the React implementation of a textfield from the [Nachos design system](https://design.trello.com/).
