# @atlaskit/updater-cli

## 2.0.0
- [major] [9d5cc39394](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d5cc39394):

  - Dropped ES5 distributables from the typescript packages

## 1.0.1
- [patch] [dd30b8f831](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/dd30b8f831):

  - Add missing dependencies

## 1.0.0
- [major] [bc47f570b2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bc47f570b2):

  - MVP of editor updater cli
